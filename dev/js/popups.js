"use strict";
jQuery(function($) {
    var popup = {
        init: function() {
            this.init_cache();
            this.api();
            this.events();
        },

        init_cache: function() {
            this.src = "https://www.youtube.com/iframe_api?";
            this.domen = window.location.hostname;
            this.$overlay = $(".js-overlay");
            this.$btn_close = $(".js-close-popup");
            this.$btn_call = $(".js-call-popup");
            this.$popups = $(".js-popup");
            this.player;
        },

        api: function() {
            var tag = document.createElement("script");
            tag.src = "https://www.youtube.com/iframe_api?enablejsapi=1";
            var firstScriptTag = document.getElementsByTagName("script")[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        },

        events: function() {
            this.$btn_close.add(popup.$overlay).click(function() {
                popup.close_popup();
            });

            this.$btn_call.click(function(e) {
                e.preventDefault();

                var $btn = $(this),
                    parameters = $btn.data("popup"),
                    $popup = $(".js-popup-" + parameters.target);

                if (popup.$popups.hasClass("_active")) {
                    popup.$popups.removeClass("_active");
                }
                console.log(parameters)
                if (parameters.target === "video") {
                    var id = parameters.id;

                    if (!popup.player) {
                        popup.video_create(id);
                    } else {
                        popup.player.playVideo();
                    }
                }

                popup.$overlay.addClass("_active");
                $popup.addClass("_active");
            });
        },

        video_create: function(id) {
            this.player = new YT.Player("js-popup-video", {
                height: "480",
                width: "720",
                videoId: id,
                playerVars: {
                    origin: this.domen,
                    modestbranding: 1,
                    autohide: 1,
                    showinfo: 0,
                    rel: 0,
                    autoplay: 1
                }
            });
        },

        close_popup: function() {
            var $popup_active = $(".js-popup._active");

            popup.$overlay.removeClass("_active");
            $popup_active.removeClass("_active");

            if (this.player) {
                this.player.pauseVideo();
            }
        }
    };

    $(window).load(function() {
        popup.init();
    });
});
