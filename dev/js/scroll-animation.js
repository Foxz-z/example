"use strict";
jQuery(function($) {
    var scroll_animation = {
        init: function() {
            this.init_cache();
            this.run_scroll_anim(this.list_arr);
        },

        init_cache: function() {
            this.$el = $(".js-scroll-anim");
            this.list_arr = this.build_block_list(this.$el);
            this.resize_timer;
        },

        build_block_list: function($el) {
            var list_arr = [];

            $el.each(function() {
                var $el = $(this),
                    list_obj = {
                        el: $el,
                        posY: $el.offset().top
                    };
                list_arr.push(list_obj);
            });
            return list_arr;
        },

        scroll_section_anim: function(arr, current_scroll) {
            var coef = $(window).height() / 1.6;

            for (var i = 0; i < arr.length; i++) {
                if (current_scroll + coef >= arr[i].posY) {
                    arr[i].el.addClass("_active");
                }
                 else {
                     arr[i].el.removeClass('_active');
                 }
            }
        },

        run_scroll_anim: function(arr) {
            var current_scroll = $(window).scrollTop();

            scroll_animation.scroll_section_anim(arr, current_scroll);

            $(window).scroll(function() {
                var new_cur_scroll = $(window).scrollTop();

                scroll_animation.scroll_section_anim(arr, new_cur_scroll);
            });
        }
    };

    $(window)
        .load(function() {
            scroll_animation.init();
        })
        .resize(function() {
            clearTimeout(scroll_animation.resize_timer);

            scroll_animation.resize_timer = setTimeout(function() {
                if (scroll_animation.$el) {
                    scroll_animation.$el.removeClass("_active");
                }

                scroll_animation.list_arr = scroll_animation.build_block_list(
                    scroll_animation.$el
                );

                scroll_animation.run_scroll_anim(scroll_animation.list_arr);
            }, 250);
        });
});
