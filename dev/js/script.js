"use strict";
jQuery(function($) {
    var $_ = {
        init: function() {
            this.initCache();
            this.events();
            this.initForms();
            this.initSliders();
            this.check_scroll();         
                    
        },

        initCache: function() {
            //$_
            this.$body = $(".js-body");
            this.$header = $(".js-header");    
            // paralax
            this.$paralaxScene = $(".js-paralax-scene");
            this.$paralaxItem = $(".js-paralax-item");

            //sliders
            this.$slider = $(".js-slider");
            this.$slide = $('.js-slide');
            this.$dots = $('.js-altdots');
            this.$dot = $('.js-altdot');
            this.$clientslider = $('.js-clienslider');
            
            this.breakpoints = {
                pre_desktop: 1750,
                med_desktop: 1500,
                pre_small_desktop: 1300,
                small_desktop: 1200,
                tablet: 1000,
                preMobile: 900,
                mobile: 700,
                mobilePreSmall: 600,
                mobileSmall: 550
            };
        },

        events: function() {
         
            $(window).scroll(function() {
                $_.check_scroll();
            });            
        },

        init_photobox: function() {
            this.$photobox_wrapper.photobox(".js-photobox-single");
        },

        check_scroll: function() {
            var cur_pos = $(window).scrollTop();
            
            if (cur_pos > 0) {
                this.$header.addClass("_active");
            } else {
                this.$header.removeClass("_active");
            }           
            if (cur_pos >= $_.$paralaxScene.offset().top && cur_pos<$_.$paralaxScene.offset().top+$_.$paralaxScene.height()) {
                var paralax_pos = (cur_pos-$_.$paralaxScene.offset().top)*0.5;
                $_.$paralaxItem.css({"transform":"translate(0px,"+paralax_pos+"px)"});
            }
            
            
        },

        initSliders: function() {
            
            function getRandomInt(min, max) {
                min = Math.ceil(min);
                max = Math.floor(max);
                return Math.floor(Math.random() * (max - min)) + min;
              }
              
            function setActiveDot(obj,index) {                 
                $(obj).find($_.$dot).removeClass('_active').eq(index).addClass('_active');
            }
            
            function setActiveSlide(obj,index) {
                $(obj).slick('slickGoTo',index);
            }
            
            this.$slider
                .on("init", function(event,slick) {                   
                    setActiveDot($_.$dots,slick.currentSlide);
                })
                .slick({
                    fade:true,
                    dots: false,
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,                    
                    lazyLoad: "progressive",  
                    initialSlide:getRandomInt(0, $_.$slider.find($_.$slide).length),
                    autoplay: true,
                    autoplaySpeed: 2000, 
                    speed: 2000
                }).on("beforeChange", function(
                event,
                slick,
                currentSlide,
                nextSlide
            ) {
                setActiveDot($_.$dots,nextSlide);             
                
            });;
            
            $_.$dot.click(function(){
               setActiveSlide($_.$slider,$(this).index()); 
            })
            
            this.$clientslider.slick({
                    dots: false,
                    arrows: false,
                    slidesToShow: 5,
                    slidesToScroll: 1,                    
                    lazyLoad: "progressive",                      
                    autoplay: true,
                    autoplaySpeed: 2000,
                    responsive:[ {
                        breakpoint:1101,
                        settings:{
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint:451,
                        settings:{
                            slidesToShow: 2
                        }
                    }
                    ]
            })
        },
        init_slider_counter: function(slick) {
                var $slider = $(slick.$slider),
                    $counter = $slider
                        .closest(".js-slider-wrapper")
                        .find(".js-slider-counter");
                $counter.find(".js-slider-counter-total").text((slick.slideCount<10)?'0'+slick.slideCount:slick.slideCount);
                $counter
                    .find(".js-slider-counter-current")
                    .text((slick.initials.currentSlide + 1 < 10)?'0'+(slick.initials.currentSlide + 1):slick.initials.currentSlide + 1);
            },
        update_slider_counter: function(slick, currentSlide) {
            //console.log("update", currentSlide);
            var $slider = $(slick.$slider),
                $counter = $slider
                    .closest(".js-slider-wrapper")
                    .find(".js-slider-counter");

            $counter.find(".js-slider-counter-current").text((currentSlide + 1<10)?'0'+(currentSlide + 1):currentSlide + 1);
        },
        fix_slick_clone_bug: function(slick, nextSlide) {
            var index = slick.$slides.eq(nextSlide).data("index"),
                $clones = slick.$slider.find(".slick-slide.slick-cloned"),
                $slide_clone = $clones.filter("[data-index=" + index + "]");

            $clones.removeClass("_active");

            if ($slide_clone.length) {
                $slide_clone.addClass("_active");
            }
        },

        initForms: function() {
            form_adjuster.init({
                success: function() {
                    var $form = $(form_adjuster.$form_cur),
                        $inputs = $form.find("input,textarea");

                    $(".js-popup._active").removeClass("_active");
                    $(".js-popup-thx")
                        .add(".js-overlay")
                        .addClass("_active");

                    setTimeout(function() {
                        $form.trigger("reset");
                        $inputs.removeClass("valid error active _active");
                    }, 500);

                    setTimeout(function() {
                        if ($(".js-popup").hasClass("_active")) {
                            $(".js-popup,.js-overlay").removeClass("_active");
                        }
                    }, 4000);
                }
            });
        },
        

        initResizeTrigger: function() {
            var resizeTimer = null,
                resizeDelay = 300,
                windowWidth = $(window).width(),
                windowHeight = $(window).height();

            $(window).resize(function() {
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function() {
                    var currentWidth = $(window).width(),
                        currentHeight = $(window).height(),
                        resizeWidth = windowWidth !== currentWidth,
                        resizeHeight = windowHeight !== currentHeight;

                    if (resizeWidth) {
                        windowWidth = currentWidth;
                        $_.windowWidth = currentWidth;

                        if (!resizeHeight)
                            $_.$body.trigger("body:resize:width");
                    }

                    if (resizeHeight) {
                        windowHeight = currentHeight;

                        if (!resizeHeight)
                            $_.$body.trigger("body:resize:height");
                    }

                    $_.$body.trigger("body:resize");
                }, resizeDelay);
            });
        }
    };

    $(document).ready(function() {
        $("img.lazy").lazyload({
            effect : "fadeIn",
            threshold: 500
        });
        $_.init();
    });
});


