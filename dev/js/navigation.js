"use strict";
jQuery(function($) {
    var nav = {
        init: function() {
            this.init_cache();
            this.events();
        },

        init_cache: function() {
            this.$hamburger = $(".js-hamburger");
            this.$nav = $(".js-nav-block");
            this.$link = $(".js-navigation-item");
            this.$menu = $(".js-menu-wrapper");
            this.$arrow = $(".js-close-menu");
            this.$header = $(".js-header");
        },

        events: function() {
            this.$link.click(function(e) {
                var $btn = $(this),
                    target = $btn.attr("data-anchor");

                if (target) {
                    e.preventDefault();

                    var $btn = $(this),
                        $target_sec = $(".js-navigation-" + target),
                        header_height = nav.$header.height(),
                        pos = $target_sec.offset().top - header_height,
                        cur_scroll = $(window).scrollTop(),
                        need_scroll = Math.abs(pos - cur_scroll);

                    nav.$hamburger.removeClass("_active");
                    nav.$nav.add(nav.$menu).removeClass("_active");

                    if ($(".js-popup").hasClass("_active")) {
                        $(".js-popup,.js-overlay").removeClass("_active");
                    }

                    $("body,html").animate(
                        {
                            scrollTop: pos
                        },
                        need_scroll / 2
                    );
                }
            });

            this.$arrow.click(function() {
                nav.$hamburger.removeClass("animate");
                nav.$nav.add(nav.$menu).removeClass("_active");
            });
        }
    };

    $(window).load(function() {
        nav.init();
    });
});
