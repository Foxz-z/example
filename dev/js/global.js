jQuery(function($) {
    function inspectInput($input) {
        $input.addClass("_active");
        var val = $input.val(),
            notValidPhone = $input.hasClass("userphone")
                ? val.replace(/[^0-9.]/g, "").length < 2
                : false;
        if ((!val || notValidPhone) && !$input.hasClass("js-datepicker"))
            $input.removeClass("_active");
    }
    $("input, textarea")
        .on("click", function() {
            $(this).addClass("_active");
        })
        .focusout(function() {
            inspectInput($(this));
        })
        .change(function() {
            inspectInput($(this));
        });
    $("input, textarea").each(function() {
        inspectInput($(this));
    });
    $("body").on("click", function(e) {
        var $el = $(e.target);

        if (
            !$el.hasClass("js-body-click-close") &&
            !$el.closest(".js-body-click-close").length
        ) {
            var $elements = $(".js-body-click-close");

            $.each($elements, function(key, item) {
                var $item = $(item);
                if (!($item.is("input") && $item.val())) {
                    $item.removeClass("_active");

                    if ($item.hasClass("js-call-callback")) {
                        $(".js-form-callback")
                            .stop(true)
                            .slideUp(500);
                    }
                }
            });
        }
    });

    $(".pseudo-hidden").on("change", function(e) {
        var $el = $(e.currentTarget);
        if ($el.val().length) $el.removeClass("error");
    });
});
