"use strict";
jQuery(function($) {
    var prx = {
        init: function() {
            this.cache();
            if (this.touch != "Mobile") this.parallax_init();
        },

        cache: function() {
            this.$scene = $(".js-parallax-scene");
            this.touch = detect.parse(navigator.userAgent).device.type;
            this.instance = [];
        },

        parallax_init: function() {
            this.$scene.each(function() {
                var $el = $(this);

                prx.instance.push(
                    new Parallax($el[0], {
                        relativeInput: true,
                        selector: ".js-parallax-element",
                        invertX: false,
                        invertY: false,
                        hoverOnly: true
                    })
                );
            });
        }
    };

    $(window).load(function() {
        prx.init();
    });
});
