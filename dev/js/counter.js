"use strict";
jQuery(function($) {
    var count = {
        init: function() {
            this.cache();
            this.events();
            this.calc_total();
        },
        cache: function() {
            this.$wrapper = $(".js-counter-block");
            this.$arrow = $(".js-counter-arrow");
            this.$input = $(".js-counter-input");
            this.$card_item = $(".js-card-item");
            this.$card_sum = $(".js-card-sum");
            this.$total = $(".js-sum-total");
            this.$crossBtn = $(".js-close-btn");
            this.$lessBtn = $(".js-less");
        },
        events: function() {
            this.$arrow.on("click", function() {
                var $item = $(this),
                    $wrapper = $item.closest(count.$wrapper),
                    $input = $wrapper.find(count.$input),
                    value = parseInt($input.val()),
                    max = $input.data("max");

                if ($item.hasClass("js-more")) {
                    if (value < max) {
                        var counter = value + 1;

                        $input.val(counter);
                        count.calc_sum($input, counter);
                    }
                } else {
                    if (value > 1) {
                        $input.val(value - 1);
                        count.calc_sum($input, value - 1);
                    }
                }
            });

            this.$input.focusout(function() {
                var $input = $(this),
                    value = parseInt($input.val());

                if (value < 1 || !value) {
                    $input.val(1);
                    count.calc_sum($input, 1);
                } else {
                    count.calc_sum($input, value);
                }
            });
            this.$crossBtn.click(function() {
                var $btn = $(this),
                    $aheadItem = $btn.closest(count.$card_item);

                $aheadItem.fadeOut(500, function() {
                    $aheadItem.remove();
                    if (count.$total.length) {
                        count.calc_total();
                    }
                });
            });
        },

        calc_sum: function($input, counter) {
            var $block = $input.closest(count.$card_item),
                $count = $block.find(count.$card_sum),
                price_one = $count.data("price");

            $count.text(count.sum_normalize(price_one * counter));

            if (this.$total.length) {
                this.calc_total();
            }
        },

        calc_total: function() {
            var total = 0;

            this.$card_sum.filter(":visible").each(function() {
                var number = parseInt(
                    $(this)
                        .text()
                        .replace(/\s/g, "")
                );

                total += number;
            });

            this.$total.text(count.sum_normalize(total));
        },

        sum_normalize: function(sum) {
            var fixedSum = sum.toFixed(2);
            return fixedSum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }
    };
    $(window).load(function() {
        count.init();
    });
});
