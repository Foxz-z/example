require("jquery"); //jquery

require("objectFitPolyfill");    
window.Parallax = require("parallax-js");

$.fn.objectFitImages = require("object-fit-images");
$.fn.objectFitImages("img.js-object-fit");

require("./plugin/jquery.validate.min.js"); //validation
require("./plugin/jquery.inputmask.bundle.min.js"); //mask
require("./plugin/jquery.lazyload.min.js");
require("./plugin/detect.min.js"); //detect browser
require("./plugin/photobox.js");
require("./plugin/slick.js"); // slick slider
require("./plugin/ion.rangeSlider.min.js");
require('./plugin/perfect-scrollbar.jquery.min');
require('./plugin/zoomsl-3.0.min.js');

require("./global.js"); //global scripts
require("./navigation.js"); //anchor scroll animate
require("./forms.js"); //forms sender scripts
require("./select.js"); //forms sender scripts
require("./popups.js"); //popups scripts
require("./y-map.js");
require("./scroll-animation.js");
require("./parallax.js");
require("./counter.js"); //merchcounter scripts
require("./script.js"); //general scripts




